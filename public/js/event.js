var eventSource = new EventSource('/events');
eventSource.onmessage = function(event) {
    const result = JSON.parse(event.data)
    document.getElementById('background').style.backgroundImage = "url('" + result.background + "')";

    if (result.type === "landingpage") {
        showContent("landingpage")
        hiddenContent("ayat")
        addText("date", result.date)
        addText("title", result.title)
        placeDiv("landingpage", 180, 700)
    } else {
        showContent("ayat")
        hiddenContent("landingpage")
        addText("ayat_title", result.ayat_title)
        addText("ayat_prase", result.ayat_prase)
        addText("ayat_translate", result.ayat_translate)
        placeDiv("ayat_title", 150, 80)
        placeDiv("ayat_prase", 200, 200)
        placeDiv("ayat_translate", 200, 650)
    }
}

function placeDiv(id, x, y) {
    var d = document.getElementById(id);
    d.style.position = "absolute";
    d.style.left = x +'px';
    d.style.top = y +'px';
}

function addText(id, text) {
    document.getElementById(id).innerHTML = text;
}

function showContent(id) {
    document.getElementById(id).style.display = "block"
}

function hiddenContent(id) {
    document.getElementById(id).style.display = "none"
}
