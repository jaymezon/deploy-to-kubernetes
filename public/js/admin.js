const listType = ["landingpage", "ayat"]

var selectType = document.getElementById('type');

for (var i = 0; i < listType.length; i++){
    var opt = document.createElement('option');
    opt.value = listType[i];
    opt.innerHTML = listType[i];
    selectType.appendChild(opt);
}

hiddenContent("ayat_ctl")

function showContent(id) {
    document.getElementById(id).style.display = "block"
}

function hiddenContent(id) {
    document.getElementById(id).style.display = "none"
}


$(function() {
    $('#type').on('change', function(event) {
        if (this.value == "landingpage") {
            showContent("landingpage_ctl")
            hiddenContent("ayat_ctl")
        } else {
            showContent("ayat_ctl")
            hiddenContent("landingpage_ctl")
        }
    });
});

function updatePage() {
    var url = "/update";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
        console.log(xhr.status);
        console.log(xhr.responseText);
    }};

    var selectType = document.getElementById('type');
    var type = selectType.options[selectType.selectedIndex].text;

    if (type === "landingpage") {
        var background = "public/images/Landing.png"
    } else {
        var background = "public/images/Background1.png"
    }

    var from;

    if (type === "ayat") {
        var selectSurah = document.getElementById('surah');
        var surah = selectSurah.selectedIndex + 1;
        var ayat = document.getElementById('ayat').value
        from = "quran"
    } else {
        var surah = 0
        var ayat = 0
        from = ""
    }

    const ayatCustom = document.getElementById('ayat_custom').value
    const translateCustom = document.getElementById('translate_custom').value
    const titleCustom = document.getElementById('title_custom').value
    if (ayatCustom.length > 0) {
        from = "custom"
        document.getElementById('ayat_custom').value = ""
        document.getElementById('translate_custom').value = ""
        document.getElementById('title_custom').value = ""
    }

    var data = `{
        "type": "${type}",
        "background": "${background}",
        "from": "${from}",
        "surah": "${surah}",
        "ayat": "${ayat}",
        "title_custom": "${titleCustom}",
        "ayat_custom": "${ayatCustom}",
        "translate_custom": "${translateCustom}"
    }`;

    xhr.send(data);
}
